// Loads dependencies and initializes this module
ModulesLoader.requireModules(['threejs/three.min.js', 'Physics.js', 'DebugHelper.js']) ;

// Add the bezier curve to the scene. TODO: Move this into another file
var curve = new THREE.CubicBezierCurve(
	new THREE.Vector3( 0, 0 ),
	new THREE.Vector3( 150, 300 ),
	new THREE.Vector3( 300, -300 ),
	new THREE.Vector3( 400, 0 )
);

// Draw the curve
var path = new THREE.Path( curve.getPoints( 50 ) );
var geometry = path.createPointsGeometry( 50 );
var material = new THREE.LineBasicMaterial( { color : 0xff0000 } );

// Create the final Object3d to add to the scene
var curveObject = new THREE.Line( geometry, material );

var compteur1 = 0;
var compteur2 = 0;

// Converts from degrees to radians.
Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};
 
// Converts from radians to degrees.
Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

/** A helicoptere  
 * 
 * @param configuration
 * @returns {helicoptere}
 */
function Helicoptere(Loader, renderingEnvironment)
{

	// Add the bezier curve
	renderingEnvironment.addToScene(curveObject);

	// Helicoptere Translation
	var helicoTranslation = new THREE.Object3D(); 
	helicoTranslation.name = 'helico0'; 
	renderingEnvironment.addToScene(helicoTranslation); 

	// initial POS
	helicoTranslation.position.x = 0;
	helicoTranslation.position.y = 0;
	helicoTranslation.position.z = 0;

	// helico Rotation floor slope follow
	var helicoFloorSlope = new THREE.Object3D(); 
	helicoFloorSlope.name = 'helico1';
	helicoTranslation.add(helicoFloorSlope);

	// helico vertical rotation
	var helicoRotationZ = new THREE.Object3D(); 
	helicoRotationZ.name = 'helico2';
	helicoFloorSlope.add(helicoRotationZ);
	helicoRotationZ.rotation.z = 0 ;

	// the helico itself 
	var helicoGeometry = Loader.load({filename: 'assets/helico/helicoCorp.obj', node: helicoRotationZ, name: 'helico3'}) ;
	helicoGeometry.position.z= +0.25 ;

	var turbineDroite = Loader.load({filename: 'assets/helico/turbine.obj', node: helicoGeometry, name: 'turbineDroite'}) ;
	turbineDroite.position.x= +8.5 ;
	turbineDroite.position.y= -3 ;
	turbineDroite.position.z= 4 ;

	var turbineGauche = Loader.load({filename: 'assets/helico/turbine.obj', node: helicoGeometry, name: 'turbineGauche'}) ;
	turbineGauche.position.x= -8.5 ;
	turbineGauche.position.y= -3 ;
	turbineGauche.position.z= 4 ;

	var turbineCentrale = Loader.load({filename: 'assets/helico/turbine.obj', node: helicoGeometry, name: 'turbineCentrale'}) ;
	turbineCentrale.position.z= +4;
	turbineCentrale.rotation.x= Math.radians(90) ;

	var axeDroit = Loader.load({filename: 'assets/helico/axe.obj', node: turbineDroite, name: 'axeDroit'}) ;
	axeDroit.position.y= +1 ;

	var axeGauche = Loader.load({filename: 'assets/helico/axe.obj', node: turbineGauche, name: 'axeGauche'}) ;
	axeGauche.position.y= +1 ;

	var axeCentral = Loader.load({filename: 'assets/helico/axe.obj', node: turbineCentrale, name: 'axeCentral'}) ;
	axeCentral.position.y= +1 ;

	var paleDroite1 = Loader.load({filename: 'assets/helico/pale.obj', node: axeDroit, name: 'paleDroite1'}) ;
	paleDroite1.position.y= +2 ;
	paleDroite1.rotation.y= Math.radians(120) ;
	var paleDroite2 = Loader.load({filename: 'assets/helico/pale.obj', node: axeDroit, name: 'paleDroite2'}) ;
	paleDroite2.position.y= +2 ;
	paleDroite2.rotation.y= Math.radians(240) ;
	var paleDroite3 = Loader.load({filename: 'assets/helico/pale.obj', node: axeDroit, name: 'paleDroite3'}) ;
	paleDroite3.position.y= +2 ;
	paleDroite3.rotation.y= Math.radians(360) ;

	var paleGauche1 = Loader.load({filename: 'assets/helico/pale.obj', node: axeGauche, name: 'paleGauche1'}) ;
	paleGauche1.position.y= +2 ;
	paleGauche1.rotation.y= Math.radians(120) ;
	var paleGauche2 = Loader.load({filename: 'assets/helico/pale.obj', node: axeGauche, name: 'paleGauche2'}) ;
	paleGauche2.position.y= +2 ;
	paleGauche2.rotation.y= Math.radians(240) ;
	var paleGauche3 = Loader.load({filename: 'assets/helico/pale.obj', node: axeGauche, name: 'paleGauche3'}) ;
	paleGauche3.position.y= +2 ;
	paleGauche3.rotation.y= Math.radians(360) ;

	var paleCentrale1 = Loader.load({filename: 'assets/helico/pale.obj', node: axeCentral, name: 'paleCentrale1'}) ;
	paleCentrale1.position.y= +2 ;
	paleCentrale1.rotation.y= Math.radians(120) ;
	var paleCentrale2 = Loader.load({filename: 'assets/helico/pale.obj', node: axeCentral, name: 'paleCentrale2'}) ;
	paleCentrale2.position.y= +2 ;
	paleCentrale2.rotation.y= Math.radians(240) ;
	var paleCentrale3 = Loader.load({filename: 'assets/helico/pale.obj', node: axeCentral, name: 'paleCentrale3'}) ;
	paleCentrale3.position.y= +2 ;
	paleCentrale3.rotation.y= Math.radians(360) ;


	// attach the scene camera to car
	// helicoGeometry.add(renderingEnvironment.camera) ;

	/*
	* Changer la position de l'hélicoptère pour le faire avancer
	*/
	this.appliquerPosition = function(position){
		helicoGeometry.position.x = position.x;
		helicoGeometry.position.y = position.y;
	}

	/* 
	* Changer l'orientation de l'hélicoptère sur l'axe Z en fonction du vecteur vitesse
	* Faisant tourner les pales en fonction de la norme de ce vecteur
	*/
	this.appliquerVitesse = function(vitesse)
	{
		// Récupération norme du vecteur
		var norme = (Math.sqrt((vitesse.x * vitesse.x) + (vitesse.y * vitesse.y)));
		
		// Rotation pales
		axeDroit.rotation.y = axeDroit.rotation.y + norme;
		axeGauche.rotation.y = axeGauche.rotation.y + norme;
		axeCentral.rotation.y = axeCentral.rotation.y + norme;

		// Rotation hélicoptère
		helicoGeometry.rotation.z = Math.atan2(vitesse.y, vitesse.x) - Math.radians(90);

		// Tracer le vecteur vitesse
		var material2 = new THREE.LineBasicMaterial({
			color: 0x6699FF,
			linewidth: 2
		});
		var geometry2 = new THREE.Geometry();
		geometry2.vertices.push(
			new THREE.Vector3( helicoGeometry.position.x,  helicoGeometry.position.y, 0 ),
			new THREE.Vector3( helicoGeometry.position.x + vitesse.x * 50, helicoGeometry.position.y + vitesse.y * 50, 0 )
		);

		var line2 = new THREE.Line( geometry2, material2 );
		compteur1++;
		if(compteur1 % 50 == 1)	renderingEnvironment.addToScene(line2);
	}

	/*
	* Orienter les turbines gauches et droites sur l'axe Z en fonction du vecteur accèlération 
	* Accélère les rotation des pales en fonction de la norme de l'accélération
	*/
	this.appliquerAcceleration = function(acceleration)
	{
		// Récupération norme du vecteur
		var norme = (Math.sqrt((acceleration.x * acceleration.x) + (acceleration.y * acceleration.y)));

		// Rotation turbines
		turbineDroite.rotation.z = Math.atan2(acceleration.y, acceleration.x) - helicoGeometry.rotation.z - Math.radians(90); 
		turbineGauche.rotation.z = Math.atan2(acceleration.y, acceleration.x) - helicoGeometry.rotation.z - Math.radians(90); 

		// Accélère les pales		
		axeDroit.rotation.y = axeDroit.rotation.y + norme * 10;
		axeGauche.rotation.y = axeGauche.rotation.y + norme * 10;
		axeCentral.rotation.y = axeCentral.rotation.y + norme * 10;

		// Tracer le vecteur acceleration
		var material2 = new THREE.LineBasicMaterial({
			color: 0xFF9933,
			linewidth: 2
		});
		var geometry2 = new THREE.Geometry();
		geometry2.vertices.push(
			new THREE.Vector3( helicoGeometry.position.x,  helicoGeometry.position.y, 0 ),
			new THREE.Vector3( helicoGeometry.position.x + acceleration.x * 500, helicoGeometry.position.y + acceleration.y * 500, 0 )
		);

		var line2 = new THREE.Line( geometry2, material2 );
		compteur2++;
		if(compteur2 % 50 == 1)	renderingEnvironment.addToScene(line2);
		
	}

	/*
	* Rendre les nodes
	* Utilisé pour lier la position des emmeteurs de particules à nos turbines
	*/
	this.getTurbineDroite = function(){
		return turbineDroite;
	}
	this.getTurbineGauche = function(){
		return turbineGauche;
	}

}
