/**
 *  ThreeJS test file using the ThreeRender class
 */

//Loads all dependencies
requirejs(['ModulesLoaderV2.js'], function()
		{ 
			// Level 0 includes
			ModulesLoader.requireModules(["threejs/three.min.js"]) ;
			ModulesLoader.requireModules([ "myJS/ThreeRenderingEnv.js", 
			                              "myJS/ThreeLightingEnv.js", 
			                              "myJS/ThreeLoadingEnv.js", 
			                              "myJS/navZ.js",
			                              "FlyingVehicle.js",
			                              "Helicoptere.js",
			                              "Interpolateur.js",
			                              "Interpolators.js",
			                              "ParticleSystem.js"]) ;
			// Loads modules contained in includes and starts main function
			ModulesLoader.loadModules(start) ;
		}
) ;

function start()
{
	//	----------------------------------------------------------------------------
	//	MAR 2014 - TP Animation hélicoptère
	//	author(s) : Cozot, R. and Lamarche, F.
	//	---------------------------------------------------------------------------- 			
	//	global vars
	//	----------------------------------------------------------------------------
	//	keyPressed
	var currentlyPressedKeys = {};

	//	rendering env
	var renderingEnvironment =  new ThreeRenderingEnv();

	//	lighting env
	var Lights = new ThreeLightingEnv('rembrandt','neutral','spot',renderingEnvironment,5000);

	//	Loading env
	var Loader = new ThreeLoadingEnv();

	// Camera setup
	renderingEnvironment.camera.position.x = 200.0 ;
	renderingEnvironment.camera.position.y = 0.0 ;
	renderingEnvironment.camera.position.z = 180.0 ;
	
	// Create the helicoptere
	var helicoptere = new Helicoptere(Loader, renderingEnvironment);

	// Create the particle system
	var particuleSyst = new ParticleSystem.Engine_Class({particlesCount: 50, textureFile: 'assets/particles/particle.png', blendingMode: THREE.AdditiveBlending})

	// Attach the particle system to the scene
	renderingEnvironment.addToScene(particuleSyst.particleSystem)

	// Add a particle emitter
	var paticuleEmit = new ParticleSystem.ConeEmitterOnNode_Class({cone: {height: new THREE.Vector3(0, -1, 0), flow: 100, center: new THREE.Vector3(0, 0, 0), radius: 0.1}, particle: {lifeTime: new MathExt.Interval_Class(1,1.2), size: new MathExt.Interval_Class(2.5,3.5), mass: new MathExt.Interval_Class(0.1,0.3), speed: new MathExt.Interval_Class(20,30)}}, helicoptere.getTurbineDroite());
	particuleSyst.addEmitter(paticuleEmit);

	// Modifiers 
	particuleSyst.addModifier(new ParticleSystem.LifeTimeModifier_Class);
	particuleSyst.addModifier(new ParticleSystem.ForceModifier_Weight_Class);
	particuleSyst.addModifier(new ParticleSystem.OpacityModifier_TimeToDeath_Class(new Interpolators.Linear_Class(1,0.3)));
	particuleSyst.addModifier(new ParticleSystem.ColorModifier_TimeToDeath_Class({r: 255, g: 255, b: 255}, {r: 128, g: 0, b: 0}));
	particuleSyst.addModifier(new ParticleSystem.PositionModifier_EulerItegration_Class);

	// Retrieve the interpolateur
	var interpolateur = new Interpolateur();

	// Defining the interval of time
	var seconds = 1000,
		second = 0,
		interval;
	interval = setInterval(function() {
		if (second > seconds) clearInterval(interval);
  		helicoptere.appliquerPosition(interpolateur.getPosition(second));
  		helicoptere.appliquerVitesse(interpolateur.getVitesse(second));
  		helicoptere.appliquerAcceleration(interpolateur.getAcceleration(second));
		second++;
    }, 10);

	//	event listener
	//	---------------------------------------------------------------------------
	//	resize window
	window.addEventListener( 'resize', onWindowResize, false );
	//	keyboard callbacks 
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;					

	//	callback functions
	//	---------------------------------------------------------------------------
	function handleKeyDown(event) { currentlyPressedKeys[event.keyCode] = true;}
	function handleKeyUp(event) {currentlyPressedKeys[event.keyCode] = false;}

	function handleKeys() {
		if (currentlyPressedKeys[67]) // (C) debug
		{
			// debug scene
			renderingEnvironment.scene.traverse(function(o){
				console.log('object:'+o.name+'>'+o.id+'::'+o.type);
			});
		}				
		var rotationIncrement = 0.05 ;
		if (currentlyPressedKeys[68]) // (D) Right
		{
			renderingEnvironment.scene.rotateOnAxis(new THREE.Vector3(0.0,1.0,0.0), rotationIncrement) ;
		}
		if (currentlyPressedKeys[81]) // (Q) Left 
		{		
			renderingEnvironment.scene.rotateOnAxis(new THREE.Vector3(0.0,1.0,0.0), -rotationIncrement) ;
		}
		if (currentlyPressedKeys[90]) // (Z) Up
		{
			renderingEnvironment.scene.rotateOnAxis(new THREE.Vector3(1.0,0.0,0.0), rotationIncrement) ;
		}
		if (currentlyPressedKeys[83]) // (S) Down 
		{
			renderingEnvironment.scene.rotateOnAxis(new THREE.Vector3(1.0,0.0,0.0), -rotationIncrement) ;
		}
	}

	//	window resize
	function  onWindowResize() 
	{
		renderingEnvironment.onWindowResize(window.innerWidth,window.innerHeight);
	}

	function render() { 
		requestAnimationFrame( render );
		handleKeys();

		// Animate the particles
		particuleSyst.animate(0.05, renderingEnvironment.renderer);

		// Rendering
		renderingEnvironment.renderer.render(renderingEnvironment.scene, renderingEnvironment.camera); 
	};

	render(); 
}
