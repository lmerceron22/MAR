var curve = new THREE.CubicBezierCurve(
	new THREE.Vector3( 0, 0 ),
	new THREE.Vector3( 150, 300 ),
	new THREE.Vector3( 300, -300 ),
	new THREE.Vector3( 400, 0 )
);

function Interpolateur()
{
	this.getPosition = function(time)
	{
	  return curve.getPoint(time/1000);
	}
	this.getVitesse = function(time)
	{
	  var vSuiv = curve.getPoint((time+5)/1000);
	  var vPrec = curve.getPoint((time-5)/1000);

	  vSuiv.sub(vPrec);
	  vSuiv.divideScalar(10);

	  return vSuiv;

	  // return curve.getTangent(time/1000);
	}

	this.getAcceleration = function(time)
	{
	  var vSuiv = this.getVitesse((time+5));
	  var vPrec = this.getVitesse((time-5));

	  var newV = {x: (vSuiv.x - vPrec.x), y: (vSuiv.y - vPrec.y)};

	  return newV;
	}
}
